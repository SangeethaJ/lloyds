package com.lloydsbank.currency.api.lloyds.resources.test;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.text.MessageFormat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.StringUtils;

import com.lloydsbank.currency.api.lloyds.currency.resources.CurrencyResource;
import com.lloydsbank.currency.api.lloyds.impl.UnknownRateException;
import com.lloydsbank.currency.api.lloyds.model.CurrencyCode;
import com.lloydsbank.currency.api.lloyds.model.CurrencyExchangeRequest;
import com.lloydsbank.currency.api.lloyds.model.CurrencyExchangeResponse;
import com.lloydsbank.currency.api.lloyds.service.CurrencyExchangeService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CurrencyResource.class, secure = false)
public class CurrencyResourceTest {
	
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private CurrencyExchangeService currencyExchangeService;

	@Test
	public void shouldReturnCorrectExchangeRateForValidCurrencyRequest() throws Exception {

		String currencyRequestJson = "{\"requestAmount\":\"10.00\",\"sourceCurrency\":\"EUR\",\"targetCurrency\":\"GBP\"}";
		String expected = "{\"sourceCurrency\": \"EUR\", \"targetCurrency\": \"GBP\",\"exchangeRate\": \"1.0 EUR is 0.90 GBP\",\"exchangeAmount\": 9}";

		CurrencyExchangeResponse currencyExchangeResponse = new CurrencyExchangeResponse();
		currencyExchangeResponse.setExchangeAmount(new BigDecimal("9"));
		currencyExchangeResponse.setExchangeRate("1.0 EUR is 0.90 GBP");
		currencyExchangeResponse.setSourceCurrency("EUR");
		currencyExchangeResponse.setTargetCurrency("GBP");

		CurrencyExchangeRequest currencyExchangeRequest = new CurrencyExchangeRequest();
		currencyExchangeRequest.setRequestAmount(new BigDecimal("10.00"));
		currencyExchangeRequest.setSourceCurrency(CurrencyCode.EUR.name());
		currencyExchangeRequest.setTargetCurrency(CurrencyCode.GBP.name());

		Mockito.when(currencyExchangeService.getCurrencyExchange(currencyExchangeRequest))
				.thenReturn(currencyExchangeResponse);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/currency/exchange")
				.accept(MediaType.APPLICATION_JSON).content(currencyRequestJson)
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();

		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);

	}

	@Test
	public void shouldThrowErrorForUnknowCurrencyRequest() throws Exception {

		String currencyRequestJson = "{\"requestAmount\":\"10.00\",\"sourceCurrency\":\"GBP\",\"targetCurrency\":\"EUR\"}";

		CurrencyExchangeRequest currencyExchangeRequest = new CurrencyExchangeRequest();
		currencyExchangeRequest.setRequestAmount(new BigDecimal("10.00"));
		currencyExchangeRequest.setSourceCurrency(CurrencyCode.GBP.name());
		currencyExchangeRequest.setTargetCurrency(CurrencyCode.EUR.name());

		Mockito.when(currencyExchangeService.getCurrencyExchange(currencyExchangeRequest))
				.thenThrow(new UnknownRateException(CurrencyCode.GBP, CurrencyCode.EUR, MessageFormat.format(
						"Rate from {0} to {1} " + "is unknown", CurrencyCode.GBP.name(), CurrencyCode.EUR.name())));

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/currency/exchange")
				.accept(MediaType.APPLICATION_JSON).content(currencyRequestJson)
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		System.out.println(result.getResponse().getContentAsString());

		String error = mockMvc.perform(requestBuilder).andExpect(MockMvcResultMatchers.status().isNotFound())
				.andReturn().getResolvedException().getMessage();

		assertTrue(error.contains("Rate from GBP to EUR is unknown"));

	}

}
