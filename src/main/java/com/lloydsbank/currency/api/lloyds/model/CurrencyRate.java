package com.lloydsbank.currency.api.lloyds.model;

import java.math.BigDecimal;

public class CurrencyRate {
	protected CurrencyCode currencyFrom;
	protected CurrencyCode currencyTo;
	protected BigDecimal rate;

	public CurrencyRate(CurrencyCode currencyFrom, CurrencyCode currencyTo, BigDecimal rate) {
		super();
		this.currencyFrom = currencyFrom;
		this.currencyTo = currencyTo;
		this.rate = rate;
	}

	public CurrencyCode getCurrencyFrom() {
		return currencyFrom;
	}

	public void setCurrencyFrom(CurrencyCode currencyFrom) {
		this.currencyFrom = currencyFrom;
	}

	public CurrencyCode getCurrencyTo() {
		return currencyTo;
	}

	public void setCurrencyTo(CurrencyCode currencyTo) {
		this.currencyTo = currencyTo;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currencyFrom == null) ? 0 : currencyFrom.hashCode());
		result = prime * result + ((currencyTo == null) ? 0 : currencyTo.hashCode());
		result = prime * result + ((rate == null) ? 0 : rate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CurrencyRate other = (CurrencyRate) obj;
		if (currencyFrom != other.currencyFrom)
			return false;
		if (currencyTo != other.currencyTo)
			return false;
		if (rate == null) {
			if (other.rate != null)
				return false;
		} else if (!rate.equals(other.rate))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CurrencyRate [currencyFrom=" + currencyFrom + ", currencyTo=" + currencyTo + ", rate=" + rate + "]";
	}

}
