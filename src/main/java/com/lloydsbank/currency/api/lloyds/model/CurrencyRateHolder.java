package com.lloydsbank.currency.api.lloyds.model;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.lloydsbank.currency.api.lloyds.impl.UnknownRateException;

public class CurrencyRateHolder {

	private final Map<CurrencyRateKey, CurrencyRate> rateMap;

	public CurrencyRateHolder(List<CurrencyRate> rates) {
		
		final Map<CurrencyRateKey, CurrencyRate> rateMap = new HashMap<CurrencyRateKey, CurrencyRate>();

		for (CurrencyRate rate : rates) {
			rateMap.put(new CurrencyRateKey(rate), rate);
		}

		this.rateMap = Collections.unmodifiableMap(rateMap);

	}

	public CurrencyRate getRate(CurrencyCode from, CurrencyCode to) throws UnknownRateException {
		if (from.equals(to)) {
			return new CurrencyRate(to, from, BigDecimal.ONE);
		}

		final CurrencyRateKey key = new CurrencyRateKey(from, to);
		if (rateMap.containsKey(key)) {
			return rateMap.get(key);
		}
		throw new UnknownRateException(from, to,
				MessageFormat.format("Rate from {0} to {1} " + "is unknown", from.name(), to.name()));
	}

	private static class CurrencyRateKey {
		private final CurrencyCode currencyFrom;
		private final CurrencyCode currencyTo;

		private CurrencyRateKey(CurrencyCode currencyFrom, CurrencyCode currencyTo) {
			this.currencyFrom = currencyFrom;
			this.currencyTo = currencyTo;
		}

		private CurrencyRateKey(CurrencyRate rate) {
			this(rate.getCurrencyFrom(), rate.getCurrencyTo());
		}

		@Override
		public boolean equals(Object o) {
			if (this == o)
				return true;
			if (!(o instanceof CurrencyRateKey))
				return false;

			CurrencyRateKey currencyRateKey = (CurrencyRateKey) o;

			if (!currencyFrom.equals(currencyRateKey.currencyFrom))
				return false;
			if (!currencyTo.equals(currencyRateKey.currencyTo))
				return false;

			return true;
		}

		@Override
		public int hashCode() {
			int result = currencyFrom != null ? currencyFrom.hashCode() : 0;
			result = 31 * result + (currencyTo != null ? currencyTo.hashCode() : 0);
			return result;
		}
	}
}
