package com.lloydsbank.currency.api.lloyds.service;

import com.lloydsbank.currency.api.lloyds.model.CurrencyExchangeRequest;
import com.lloydsbank.currency.api.lloyds.model.CurrencyExchangeResponse;

public interface CurrencyExchangeService {
	CurrencyExchangeResponse getCurrencyExchange(CurrencyExchangeRequest currencyExchangeRequest);
}
