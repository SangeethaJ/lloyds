package com.lloydsbank.currency.api.lloyds.model;

import java.math.BigDecimal;

public class CurrencyExchangeRequest {
	private String sourceCurrency;
	private String targetCurrency;
	private BigDecimal requestAmount;
	public String getSourceCurrency() {
		return sourceCurrency;
	}
	public void setSourceCurrency(String sourceCurrency) {
		this.sourceCurrency = sourceCurrency;
	}
	public String getTargetCurrency() {
		return targetCurrency;
	}
	public void setTargetCurrency(String targetCurrency) {
		this.targetCurrency = targetCurrency;
	}
	public BigDecimal getRequestAmount() {
		return requestAmount;
	}
	public void setRequestAmount(BigDecimal requestAmount) {
		this.requestAmount = requestAmount;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((requestAmount == null) ? 0 : requestAmount.hashCode());
		result = prime * result + ((sourceCurrency == null) ? 0 : sourceCurrency.hashCode());
		result = prime * result + ((targetCurrency == null) ? 0 : targetCurrency.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CurrencyExchangeRequest other = (CurrencyExchangeRequest) obj;
		if (requestAmount == null) {
			if (other.requestAmount != null)
				return false;
		} else if (!requestAmount.equals(other.requestAmount))
			return false;
		if (sourceCurrency == null) {
			if (other.sourceCurrency != null)
				return false;
		} else if (!sourceCurrency.equals(other.sourceCurrency))
			return false;
		if (targetCurrency == null) {
			if (other.targetCurrency != null)
				return false;
		} else if (!targetCurrency.equals(other.targetCurrency))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "CurrencyExchangeRequest [sourceCurrency=" + sourceCurrency + ", targetCurrency=" + targetCurrency
				+ ", requestAmount=" + requestAmount + "]";
	}
	
	
}
