package com.lloydsbank.currency.api.lloyds.impl;

import java.math.RoundingMode;
import java.text.MessageFormat;

import org.springframework.stereotype.Service;

import com.lloydsbank.currency.api.lloyds.model.CurrencyCode;
import com.lloydsbank.currency.api.lloyds.model.CurrencyExchangeRequest;
import com.lloydsbank.currency.api.lloyds.model.CurrencyExchangeResponse;
import com.lloydsbank.currency.api.lloyds.model.CurrencyRate;
import com.lloydsbank.currency.api.lloyds.model.CurrencyRateHolder;
import com.lloydsbank.currency.api.lloyds.service.CurrencyExchangeService;

@Service
public class CurrencyExchangeServiceImpl implements CurrencyExchangeService {
	
	public CurrencyExchangeResponse getCurrencyExchange(CurrencyExchangeRequest currencyExchangeRequest) throws UnknownRateException{
		
		CurrencyRate rate = CurrencyUtil.getCurrencyRate(currencyExchangeRequest);
			
		CurrencyExchangeResponse currencyExchangeResponse = new CurrencyExchangeResponse();
		currencyExchangeResponse.setSourceCurrency(currencyExchangeRequest.getSourceCurrency());
		currencyExchangeResponse.setTargetCurrency(currencyExchangeRequest.getTargetCurrency());
		currencyExchangeResponse.setExchangeAmount(
				currencyExchangeRequest.getRequestAmount().multiply(rate.getRate()).setScale(2, RoundingMode.HALF_UP));
		currencyExchangeResponse.setExchangeRate(
				MessageFormat.format("1.0 {0} is {1} {2}", currencyExchangeRequest.getSourceCurrency(),
						rate.getRate().toString(), currencyExchangeRequest.getTargetCurrency()));
		return currencyExchangeResponse;
		
	}

}
