package com.lloydsbank.currency.api.lloyds.model;

import java.math.BigDecimal;

public class CurrencyExchangeResponse {
	private String sourceCurrency;
	private String targetCurrency;
	private BigDecimal exchangedAmount;
	private String exchangeRate;

	public String getSourceCurrency() {
		return sourceCurrency;
	}

	public void setSourceCurrency(String sourceCurrency) {
		this.sourceCurrency = sourceCurrency;
	}

	public String getTargetCurrency() {
		return targetCurrency;
	}

	public void setTargetCurrency(String targetCurrency) {
		this.targetCurrency = targetCurrency;
	}

	public BigDecimal getExchangeAmount() {
		return exchangedAmount;
	}

	public void setExchangeAmount(BigDecimal exchangeAmount) {
		this.exchangedAmount = exchangeAmount;
	}

	public String getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((exchangedAmount == null) ? 0 : exchangedAmount.hashCode());
		result = prime * result + ((exchangeRate == null) ? 0 : exchangeRate.hashCode());

		result = prime * result + ((sourceCurrency == null) ? 0 : sourceCurrency.hashCode());
		result = prime * result + ((targetCurrency == null) ? 0 : targetCurrency.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CurrencyExchangeResponse other = (CurrencyExchangeResponse) obj;
		if (exchangedAmount == null) {
			if (other.exchangedAmount != null)
				return false;
		} else if (!exchangedAmount.equals(other.exchangedAmount))
			return false;
		if (exchangeRate == null) {
			if (other.exchangeRate != null)
				return false;
		} else if (!exchangeRate.equals(other.exchangeRate))
			return false;
		if (sourceCurrency == null) {
			if (other.sourceCurrency != null)
				return false;
		} else if (!sourceCurrency.equals(other.sourceCurrency))
			return false;
		if (targetCurrency == null) {
			if (other.targetCurrency != null)
				return false;
		} else if (!targetCurrency.equals(other.targetCurrency))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CurrencyExchange [sourceCurrency=" + sourceCurrency + ", targetCurrency=" + targetCurrency
				+ ", exchangeAmount=" + exchangedAmount + ", exchangeRate=" + exchangeRate + "]";
	}

}
