package com.lloydsbank.currency.api.lloyds.impl;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import com.lloydsbank.currency.api.lloyds.model.CurrencyCode;
import com.lloydsbank.currency.api.lloyds.model.CurrencyExchangeRequest;
import com.lloydsbank.currency.api.lloyds.model.CurrencyRate;
import com.lloydsbank.currency.api.lloyds.model.CurrencyRateHolder;

public abstract class CurrencyUtil {

	private static List<CurrencyRate> currencyRates;
	private static CurrencyRateHolder rateHolder;
	static {
		
		CurrencyRate eurToGbp = new CurrencyRate(CurrencyCode.EUR, CurrencyCode.GBP, new BigDecimal("0.90"));
		CurrencyRate eurToUsd = new CurrencyRate(CurrencyCode.EUR, CurrencyCode.USD, new BigDecimal("1.20"));
		CurrencyRate eurToJpy = new CurrencyRate(CurrencyCode.EUR, CurrencyCode.JPY, new BigDecimal("1000.01"));
		CurrencyRate eurToChf = new CurrencyRate(CurrencyCode.EUR, CurrencyCode.CHF, new BigDecimal("1.11"));
		currencyRates = Arrays.asList(eurToGbp, eurToUsd, eurToJpy, eurToChf);
		rateHolder = new CurrencyRateHolder(currencyRates);
	}

	public static CurrencyRateHolder getRateHolder() {
		return rateHolder;
	}

	public static List<CurrencyRate> getCurrencyRates() {
		return currencyRates;
	}
	
	public static CurrencyRate getCurrencyRate(CurrencyExchangeRequest currencyExchangeRequest) {		
		CurrencyRate rate = rateHolder.getRate(CurrencyCode.valueOf(currencyExchangeRequest.getSourceCurrency()),
				CurrencyCode.valueOf(currencyExchangeRequest.getTargetCurrency()));
		return rate;		
	}

}
