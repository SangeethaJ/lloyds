package com.lloydsbank.currency.api.lloyds.impl;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.lloydsbank.currency.api.lloyds.model.CurrencyCode;

@ResponseStatus(code=HttpStatus.NOT_FOUND)
public class UnknownRateException extends RuntimeException {
	
    private final CurrencyCode fromCurrency;
    private final CurrencyCode toCurrency;

    public UnknownRateException(CurrencyCode fromCurrency, CurrencyCode toCurrency) {
        this.fromCurrency = fromCurrency;
        this.toCurrency = toCurrency;
    }

    public UnknownRateException(CurrencyCode fromCurrency, CurrencyCode toCurrency, String message) {
        super(message);
        this.fromCurrency = fromCurrency;
        this.toCurrency = toCurrency;
    }

    public UnknownRateException(CurrencyCode fromCurrency, CurrencyCode toCurrency, String message, Throwable cause) {
        super(message, cause);
        this.fromCurrency = fromCurrency;
        this.toCurrency = toCurrency;
    }

    public UnknownRateException(CurrencyCode fromCurrency, CurrencyCode toCurrency, Throwable cause) {
        super(cause);
        this.fromCurrency = fromCurrency;
        this.toCurrency = toCurrency;
    }

    public CurrencyCode getFromCurrency() {
        return fromCurrency;
    }

    public CurrencyCode getToCurrency() {
        return toCurrency;
    }
}
