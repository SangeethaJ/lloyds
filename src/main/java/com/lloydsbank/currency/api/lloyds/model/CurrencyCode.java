package com.lloydsbank.currency.api.lloyds.model;

public enum CurrencyCode {
	EUR("EUR"), GBP("GBP"), CHF("CHF"), USD("USD"), JPY("JPY");

	private String currencyCode;

	private CurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
}
