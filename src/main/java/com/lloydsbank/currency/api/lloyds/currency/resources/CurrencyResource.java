package com.lloydsbank.currency.api.lloyds.currency.resources;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.lloydsbank.currency.api.lloyds.impl.UnknownRateException;
import com.lloydsbank.currency.api.lloyds.model.CurrencyExchangeRequest;
import com.lloydsbank.currency.api.lloyds.model.CurrencyExchangeResponse;
import com.lloydsbank.currency.api.lloyds.service.CurrencyExchangeService;

@RestController
public class CurrencyResource {

	@Resource
	CurrencyExchangeService currencyExchangeService;

	@RequestMapping(path = "/api/currency/exchange", method = RequestMethod.POST)
	@ResponseBody
	public CurrencyExchangeResponse getExchangeRate(@RequestBody CurrencyExchangeRequest currencyExchangeRequest) {
		return currencyExchangeService.getCurrencyExchange(currencyExchangeRequest);
	}

}
